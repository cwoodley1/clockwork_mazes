package clockwork.mazes.clockworkmazes;

import java.net.InetAddress;
import java.net.Socket;
import java.net.HttpURLConnection;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

// Only for testing, can remove later
import java.util.Random;

public class viewStats extends Activity{

	// Info for server connection
	Socket serverSocket;
	String serverName;
	int portNumber;
	
	// TextViews for the player's statistics
	TextView gearBalance;
	TextView gamesPlayed;
	TextView gamesWon;
	TextView mostMoves;
	TextView fewestMoves;
	TextView longestStreak;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_statistics);
        
        displayStatistics();
        
        serverName = "test server";
        portNumber = 9092;
        
        //makeConnection();
        //displayStatistics();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    private void makeConnection(){
    	try{
    		serverSocket = new Socket( InetAddress.getByName( serverName ), portNumber );
    		gearBalance.setText("SUCCEED port " + portNumber);
    	}
    	catch( Exception e ){
    		gearBalance.setText("FAILED port " + portNumber);
    	}
    }
    
    private void displayStatistics() {
    	// Display player's statistics in TextViews
    	// For now, using randomly-generated values just to show that they are dynamic
    	Random gen = new Random();
        gearBalance = (TextView) findViewById(R.id.gearBalanceNum);
        gearBalance.setText(Integer.toString(gen.nextInt()));
        
        gamesPlayed = (TextView) findViewById(R.id.gamesPlayedNum);
        gamesPlayed.setText(Integer.toString(gen.nextInt()));
        
        gamesWon = (TextView) findViewById(R.id.gamesWonNum);
        gamesWon.setText(Integer.toString(gen.nextInt()));
        
        mostMoves = (TextView) findViewById(R.id.mostMovesNum);
        mostMoves.setText(Integer.toString(gen.nextInt()));
        
        fewestMoves = (TextView) findViewById(R.id.fewestMovesNum);
        fewestMoves.setText(Integer.toString(gen.nextInt()));
        
        longestStreak = (TextView) findViewById(R.id.longestStreakNum);
        longestStreak.setText(Integer.toString(gen.nextInt()));
    }
}
