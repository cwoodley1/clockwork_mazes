package com.clockworkmazes;

import java.net.*;
import java.io.*;

public class Main {

	public static void main(String[] args) {

		ServerSocket serverSocket;
		ServerCtl serverCtl;
		DatabaseCtl databaseCtl;
		int clientNum = 1;

		try {
			serverSocket = new ServerSocket( 9092 );
			serverCtl = new ServerCtl();
			serverCtl.start();
			databaseCtl = new DatabaseCtl();

			System.out.println( "Server started" );

			while (true) {
				try {
					// listening for a connection
					Socket clientSocket = serverSocket.accept();

					//System.out.println("Started a thread for client #" + clientNum );
					//System.out.println("Host name: " + clientSocket.getInetAddress().getHostName() );
					//System.out.println("IP address: " + clientSocket.getInetAddress().getHostAddress() );

					DataOutputStream dos = new DataOutputStream(clientSocket.getOutputStream());
					DataInputStream dis = new DataInputStream(clientSocket.getInputStream());

					ClientThread t = new ClientThread( clientSocket, clientNum, serverCtl, databaseCtl, dos, dis);
					t.start();
					serverCtl.addClient(t);
					clientNum++;

				} catch(IOException e ) {
					e.printStackTrace();
					System.out.println( "Server failed to accept connection" );
				}
			}
		} catch(IOException e ) {
			e.printStackTrace();
			System.out.println( "Server failed to launch the listener" );
		} catch (ClassNotFoundException e) {
			// Database failed to load
			System.out.println(e.getMessage());
		}

		System.out.println( "Server stopped" );
	}
}