package com.clockworkmazes;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ClientThread extends Thread {
	private int clientNum;
	private ServerCtl serverCtl;
	private DatabaseCtl databaseCtl;
	private Socket connection;
	private ArrayList<NetworkMessage> messageList = new ArrayList<NetworkMessage>();
	private DataOutputStream dosToClient;
	private DataInputStream disFromClient;

	public ClientThread(Socket sock, int clientNum, ServerCtl serverCtl, DatabaseCtl databaseCtl,
	                    DataOutputStream dos, DataInputStream dis) {
		connection = sock;
		this.clientNum = clientNum;
		this.serverCtl = serverCtl;
		this.databaseCtl = databaseCtl;
		dosToClient = dos;
		disFromClient = dis;
	}

	public synchronized void sendMessage(String msg) {
		messageList.add(new NetworkMessage(clientNum, msg));
		notifyAll();
	}

	private synchronized String getNextMessageFromQueue() throws InterruptedException {
		while (messageList.size()==0)
			wait();
		String message = messageList.get(0).getMsg();
		messageList.remove(0);
		return message;
	}

	private void sendMessageToClient(String aMessage) throws IOException {
		dosToClient.writeUTF(aMessage);
		dosToClient.flush();
	}

    public int getClientNum() {
        return clientNum;
    }

    public void setClientNum(int clientNum) {
        this.clientNum = clientNum;
    }

	public void run() {
		try {
			try {
				while (true) {
					if(messageList.size() != 0){
						String message = getNextMessageFromQueue();
						sendMessageToClient(message);
					} else if (disFromClient.available() > 0) {
						String clientMsg = disFromClient.readUTF();
						System.out.println( "Received msg from the client: " + clientMsg );
						serverCtl.dispatchMessage(clientNum, clientMsg);
					}
				}
			} catch( EOFException eof ) {
				System.out.println("Client has terminated connection");
			} catch (IOException e) {
                System.out.println("Client has terminated connection");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

			dosToClient.close();
			disFromClient.close();
			connection.close();
			serverCtl.deleteClient(this);
		} catch(IOException e) {
            System.out.println("Error closing client connection");
			e.printStackTrace();
		}

	}
}