package com.clockworkmazes;

import java.util.*;

public class ServerCtl extends Thread {
	private ArrayList<NetworkMessage> messageQueue = new ArrayList<NetworkMessage>();
	private ArrayList<ClientThread> clientList = new ArrayList<ClientThread>();

	public synchronized void dispatchMessage(int clientNum, String msg) {
		msg = Integer.toString(clientNum) + ": " + msg;
		messageQueue.add(new NetworkMessage(clientNum, msg));
		notifyAll();
	}

	private synchronized String getNextMessageFromQueue() throws InterruptedException {

		while (messageQueue.size()==0)
			wait();
		String message = messageQueue.get(0).getMsg();
		messageQueue.remove(0);
		return message;
	}

	private synchronized void sendMessageToAllClients(String msg) {
		for (int i=0; i< clientList.size(); i++) {
			ClientThread client = clientList.get(i);
			if (client.getClientNum() != Integer.parseInt(msg.substring(0,1)) )
                client.sendMessage(msg);
		}
	}

	public synchronized void addClient(ClientThread client) {
		clientList.add(client);
	}

	public synchronized void deleteClient(ClientThread client) {

		int clientIndex = clientList.indexOf(client);
		if (clientIndex != -1)
			clientList.remove(clientIndex);
	}

    public void run() {
		try {
			while (true) {
				String message = getNextMessageFromQueue();
				sendMessageToAllClients(message);
			}
		} catch (InterruptedException ie) {
			System.out.println( "Thread interrupted" );
		}
	}
}
