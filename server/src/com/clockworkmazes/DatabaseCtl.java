package com.clockworkmazes;

import java.sql.*;

public class DatabaseCtl {

	Connection conn = null;
	Statement statement = null;
	ResultSet resultSet = null;

	DatabaseCtl() throws ClassNotFoundException {
		DBConnection db = new DBConnection("server.ini");
		conn = db.getConnection();
		if (conn == null)
			throw new ClassNotFoundException("Server can't connect to the database");
	}

	public ResultSet runQuery(String sql) {
		try {
			Statement query = conn.createStatement();
			ResultSet rs = query.executeQuery(sql);
			return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
