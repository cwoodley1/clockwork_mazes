package clockwork.mazes.clockworkmazes;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class newGame extends Activity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_game);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    public void smallMaze(View v){
    	Button mediumMaze = (Button) findViewById(R.id.mediumMaze);
    	mediumMaze.setBackgroundColor(Color.parseColor("#42160B"));
    	Button largeMaze = (Button) findViewById(R.id.largeMaze);
    	largeMaze.setBackgroundColor(Color.parseColor("#42160B"));
    	Button smallMaze = (Button) findViewById(R.id.smallMaze);
    	smallMaze.setBackgroundColor(Color.YELLOW);
    }
    public void mediumMaze(View v){
    	Button largeMaze = (Button) findViewById(R.id.largeMaze);
    	largeMaze.setBackgroundColor(Color.parseColor("#42160B"));
    	Button smallMaze = (Button) findViewById(R.id.smallMaze);
    	smallMaze.setBackgroundColor(Color.parseColor("#42160B"));
    	Button mediumMaze = (Button) findViewById(R.id.mediumMaze);
    	mediumMaze.setBackgroundColor(Color.YELLOW);
    }
    public void largeMaze(View v){
    	Button mediumMaze = (Button) findViewById(R.id.mediumMaze);
    	mediumMaze.setBackgroundColor(Color.parseColor("#42160B"));
    	Button smallMaze = (Button) findViewById(R.id.smallMaze);
    	smallMaze.setBackgroundColor(Color.parseColor("#42160B"));
    	Button largeMaze = (Button) findViewById(R.id.largeMaze);
    	largeMaze.setBackgroundColor(Color.YELLOW);
    }
    public void upPlay(View v){}
    public void downPlay(View v){}
    public void upBot(View v){}
    public void downBot(View v){}
    
}