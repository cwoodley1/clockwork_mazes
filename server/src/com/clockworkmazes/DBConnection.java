package com.clockworkmazes;

import java.sql.*;
import java.io.*;
import java.util.*;

public class DBConnection {
	//public static final String filePath = "server.ini";

	private Connection conn;
	private FileInputStream fileIn;
	private Properties propertyFile;

	public DBConnection(String filePath) {
		conn = null;
		propertyFile = new Properties();

		try {
			fileIn = new FileInputStream(filePath);
			propertyFile.load(fileIn);
		} catch (FileNotFoundException e) {
			System.out.println("FileNotFoundException: " + e.getMessage());
			e.printStackTrace();
		} catch (IOException ex) {
			System.out.println("IOException: " + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public Connection getConnection() {
		String driver = null;
		String dbUrl = null;
		String username = null;
		String password  = null;

		driver = getDriverFromPropertiesFile();
		dbUrl = getUrlFromPropertiesFile();
		username = getUserNameFromPropertiesFile();
		password = getPasswordFromPropertiesFile();

		return connect(driver, dbUrl, username, password);
	}

	private Connection connect(String driver, String dbUrl,
	                           String username, String password) {
		try {
			Class.forName(driver);

			try {
				conn = DriverManager.getConnection(dbUrl, username, password);

				return conn;
			} catch (SQLException se) {
				System.out.println("SQLException: " + se.getMessage());
				se.printStackTrace();

				return null;
			}
		} catch (ClassNotFoundException e) {
			System.out.println("ClassNotFound: " + e.getMessage());

			return null;
		}
	}

	private String getDriverFromPropertiesFile() {
		return propertyFile.getProperty("DBDriver");
	}

	private String getUrlFromPropertiesFile() {
		return propertyFile.getProperty("DBUrl");
	}

	private String getUserNameFromPropertiesFile() {
		return propertyFile.getProperty("DBUserName");
	}

	private String getPasswordFromPropertiesFile() {
		return propertyFile.getProperty("DBPassword");
	}
}

