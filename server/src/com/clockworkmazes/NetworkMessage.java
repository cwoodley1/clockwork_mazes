package com.clockworkmazes;

public class NetworkMessage {
    private int clientNum;
    private String msg;

    public NetworkMessage(int clientNum, String msg){
        this.clientNum = clientNum;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getClientNum() {
        return clientNum;
    }

    public void setClientNum(int clientNum) {
        this.clientNum = clientNum;
    }
}
