/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package clockwork.mazes.clockworkmazes;

public final class R {
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f040000;
        public static final int activity_vertical_margin=0x7f040001;
    }
    public static final class drawable {
        public static final int backgrounded=0x7f020000;
        public static final int ic_launcher=0x7f020001;
        public static final int main_menu=0x7f020002;
    }
    public static final class id {
        public static final int TextView01=0x7f080008;
        public static final int TextView02=0x7f08000a;
        public static final int TextView04=0x7f08000b;
        public static final int TextView05=0x7f080009;
        public static final int TextView06=0x7f08000d;
        public static final int TextView07=0x7f08000c;
        public static final int TextView08=0x7f080010;
        public static final int TextView09=0x7f08000e;
        public static final int action_settings=0x7f080025;
        public static final int btnDownBot=0x7f080007;
        public static final int btnDownPlay=0x7f080006;
        public static final int btnUpBot=0x7f080012;
        public static final int btnUpPlay=0x7f080004;
        public static final int button3=0x7f080003;
        public static final int button4=0x7f080001;
        public static final int button5=0x7f080005;
        public static final int button6=0x7f080002;
        public static final int fewestMoves=0x7f080021;
        public static final int fewestMovesNum=0x7f080022;
        public static final int gamesPlayed=0x7f08001b;
        public static final int gamesPlayedNum=0x7f08001c;
        public static final int gamesWon=0x7f08001d;
        public static final int gamesWonNum=0x7f08001e;
        public static final int gearBalance=0x7f080019;
        public static final int gearBalanceNum=0x7f08001a;
        public static final int header=0x7f080017;
        public static final int largeMaze=0x7f080015;
        public static final int longestStreak=0x7f080023;
        public static final int longestStreakNum=0x7f080024;
        public static final int mediumMaze=0x7f080014;
        public static final int mostMoves=0x7f08001f;
        public static final int mostMovesNum=0x7f080020;
        public static final int scrollView1=0x7f080018;
        public static final int smallMaze=0x7f080016;
        public static final int textView1=0x7f080000;
        public static final int textViewBoard=0x7f080013;
        public static final int tvBot=0x7f08000f;
        public static final int tvPlay=0x7f080011;
    }
    public static final class layout {
        public static final int activity_main=0x7f030000;
        public static final int credits=0x7f030001;
        public static final int delete_account=0x7f030002;
        public static final int delete_game=0x7f030003;
        public static final int new_game=0x7f030004;
        public static final int resume_game=0x7f030005;
        public static final int tutorial=0x7f030006;
        public static final int view_statistics=0x7f030007;
    }
    public static final class menu {
        public static final int main=0x7f070000;
    }
    public static final class string {
        public static final int AICount=0x7f050015;
        public static final int action_settings=0x7f050001;
        public static final int app_name=0x7f050000;
        public static final int carolyn=0x7f05000b;
        public static final int chris=0x7f05000f;
        public static final int credits=0x7f050006;
        public static final int deleteAccount=0x7f050007;
        public static final int deleteGame=0x7f050005;
        public static final int developers=0x7f050011;
        public static final int graphics=0x7f050012;
        public static final int hello_world=0x7f050002;
        public static final int ivan=0x7f050010;
        public static final int jonathan=0x7f05000c;
        public static final int justin=0x7f05000e;
        public static final int lblAI=0x7f050016;
        public static final int lblPlayers=0x7f050017;
        public static final int mainMenu=0x7f050009;
        public static final int music=0x7f050013;
        public static final int natesh=0x7f05000d;
        public static final int newGame=0x7f050003;
        public static final int resumeGame=0x7f050004;
        public static final int sizeBoard=0x7f050018;
        public static final int statsFewestMoves=0x7f05001c;
        public static final int statsGamesPlayed=0x7f05001a;
        public static final int statsGamesWon=0x7f05001b;
        public static final int statsGearBalance=0x7f050019;
        public static final int statsMenuHeader=0x7f05001f;
        public static final int statsMostMoves=0x7f05001d;
        public static final int statsStreak=0x7f05001e;
        public static final int title_activity_new_game=0x7f05000a;
        public static final int tutorial=0x7f050014;
        public static final int viewStats=0x7f050008;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f060000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f060001;
    }
}
